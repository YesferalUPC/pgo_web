/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.web.util;

import com.pruebitasgo.core.util.UtilCore;
import java.text.MessageFormat;
import static java.text.MessageFormat.format;
import java.util.Date;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author yesferal
 */
public class UtilWeb {
	
    private UtilWeb() {
    }

    public static void mensajeError(String codigo, String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, codigo, mensaje));
    }

    public static void mensajeInformacion(String codigo, String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, codigo, mensaje));
    }

    public static void mensajeAdvertencia(String codigo, String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, codigo, mensaje));
    }
    
    public static final String obtenerPropiedad(String clave, Object... parametros) {
        ResourceBundle rs = ResourceBundle.getBundle("messages");
        return MessageFormat.format(rs.getString(clave), parametros);
    }
    
    
    public static final void agregarObjetoSesion(String nombre, Object objeto){
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    	session.setAttribute(nombre, objeto);
    }
    
    public static final Object obtenerObjetoSesion(String nombre){
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    	return session.getAttribute(nombre);
    }
    
    public static final void removerObjetoSesion(String nombre){
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    	session.removeAttribute(nombre);
    }

    public static String controlarError(Exception exception, final Logger log) {
        StringBuilder sb = new StringBuilder();
        String idError = idError();
        sb.append(format("Código de error: {0} \n", idError));
        sb.append(format("Mensaje de error: {0} \n", exception.getMessage()));
        if (exception.getCause() != null) {
            StackTraceElement[] elem = exception.getCause().getStackTrace();
            for (StackTraceElement ex : elem) {
                sb.append(format("Clase ''{0}'' en la linea ''{1}'' en el método ''{2}'' \n",
                        ex.getClassName(), ex.getLineNumber(), ex.getMethodName()));
            }
        }
        sb.append("Imprimir toda la traza del error: \n");
        log.error(sb.toString(), exception);
        return "Ocurrior un error inesperado, código de error: " + idError;
    }
    
    public static void redireccionar(String ruta, final Logger log) { 
    	HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
    	try {
    		FacesContext.getCurrentInstance().responseComplete();
    		response.sendRedirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + ruta);
		} catch (Exception e) {
			controlarError(e, log);
		}
    }
    
    private static final String idError() {
        return UtilCore.convertirDate(new Date(), "ddMMyyyyhhmmss");
    }
}
