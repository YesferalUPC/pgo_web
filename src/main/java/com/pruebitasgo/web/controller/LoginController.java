/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.web.controller;

import com.pruebitasgo.core.dao.UsuarioDao;
import com.pruebitasgo.core.entity.Usuario;
import com.pruebitasgo.web.util.UtilWeb;
import java.io.Console;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author yesferal
 */
@ManagedBean
@SessionScoped
public class LoginController implements Serializable{
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);
    private Usuario usuario = new Usuario();
    private UsuarioDao usuarioDao = new UsuarioDao();

    public String iniciarSesion(){
        String ruta = "";
        try {
                Usuario usuarioInicio = usuarioDao.obtener(usuario.getUsuario_correo(), usuario.getUsuario_clave());
                if(usuarioInicio!=null){
                        UtilWeb.agregarObjetoSesion("usuarioInicio", usuarioInicio);
                        ruta = "/producto/home.xhtml?faces-redirect=true";
                }else{
                    UtilWeb.mensajeAdvertencia(
                    UtilWeb.obtenerPropiedad("login.incorrecto"), 
                    UtilWeb.obtenerPropiedad("login.incorrecto"));
                }
        } catch (Exception exception) {
                String mensaje = UtilWeb.controlarError(exception, LOGGER);
                UtilWeb.mensajeError(mensaje, mensaje);
        }
        return ruta;
    }
    
    public String cerrarSesion(){
        String ruta = "";
        try {
                UtilWeb.removerObjetoSesion("usuarioInicio");
                ruta = "/login.xhtml?faces-redirect=true";
                UtilWeb.mensajeError("Sesion Terminada", "Sesion terminada");
        } catch (Exception e) {
                String mensaje = UtilWeb.controlarError(e, LOGGER);
                UtilWeb.mensajeError(mensaje, mensaje);
        }
        return ruta;
    }    
    
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario Usuario) {
        this.usuario = Usuario;
    }
}
