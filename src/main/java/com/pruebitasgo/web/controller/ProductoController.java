/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.web.controller;

import com.pruebitasgo.core.dao.CategoriaDao;
import com.pruebitasgo.core.dao.ProductoDao;
import com.pruebitasgo.core.entity.Categoria;
import com.pruebitasgo.core.entity.Producto;
import com.pruebitasgo.web.util.UtilWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;

/**
 *
 * @author yesferal
 */
@ManagedBean
@SessionScoped
public class ProductoController implements Serializable{
    private static final Logger LOGGER = Logger.getLogger(ProductoController.class);
    private static final long serialVersionUID = 1L;
    private String filtro = "";
    private List<Producto> productos;
    private ProductoDao DAO = new ProductoDao();
    private CategoriaDao cDAO = new CategoriaDao();
    private Producto productoSeleccionado;
    private Producto productoGuardar;
    private List<SelectItem> categorias = null;

    public void init(){
        try {
            this.productos = DAO.listar("");
            this.productoSeleccionado = new Producto();
            this.productoGuardar = new Producto();
            this.filtro = "";
        inicializarCategorias();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    private void inicializarCategorias(){
        try {
            this.categorias = new ArrayList<SelectItem>();
            List<Categoria> listaCategorias = cDAO.listar("");
            this.productoGuardar.setCategoria_id(listaCategorias.get(0).getCategoria_id());
            for(Categoria categoria : listaCategorias){
                    this.categorias.add(new SelectItem(categoria.getCategoria_id(), categoria.getCategoria_nombre()));
            }
        } catch (Exception exception) {
            String mensaje = UtilWeb.controlarError(exception, LOGGER);
            UtilWeb.mensajeError(mensaje, mensaje);
        }
    }
           
    public String detalleProducto(){
        String ruta="";
        this.inicializarCategorias();
        if(this.productoSeleccionado!=null && this.productoSeleccionado.getProducto_nombre().length()>=0){
            this.productoGuardar = this.productoSeleccionado;
            //this.listaProducto.clear();
            //this.filtro = "";
            ruta = "/producto/actualizar.xhtml?faces-redirect=true&id=" + productoGuardar.getProducto_id();
        }else{
            UtilWeb.mensajeAdvertencia(UtilWeb.obtenerPropiedad("productoController.noSeleccionado"), UtilWeb.obtenerPropiedad("productoController.noSeleccionado"));
        }
        return ruta;
                
    } 
    
    public void actualizarProducto(){
         try {
                this.DAO.actualizar(this.productoGuardar);
                listarProductos();
                UtilWeb.mensajeInformacion(UtilWeb.obtenerPropiedad("productoController.actualizarExito", this.productoGuardar.getProducto_id()), UtilWeb.obtenerPropiedad("productoController.actualizarExito",this.productoGuardar.getProducto_id()));
            } catch (Exception exception) {
                String mensaje = UtilWeb.controlarError(exception, LOGGER);
                UtilWeb.mensajeError(mensaje, mensaje);
            }        
    } 
    
    public String nuevoProducto(){
        init();
        return "/producto/insertar.xhtml?faces-redirect=true";
    } 
    
    public void insertarProducto(){
         try {
            this.DAO.insertar(this.productoGuardar);
            UtilWeb.mensajeInformacion(UtilWeb.obtenerPropiedad("productoController.guardarExito",this.productoGuardar.getProducto_id()), UtilWeb.obtenerPropiedad("productoController.guardarExito",this.productoGuardar.getProducto_id()));            
        } catch (Exception exception) {
            String mensaje = UtilWeb.controlarError(exception, LOGGER);
            UtilWeb.mensajeError(mensaje, mensaje);
        }
        nuevoProducto();
    }
    
    public void eliminarProducto(){
        try {
            if(this.productoSeleccionado!=null && this.productoSeleccionado.getProducto_nombre().length()>=0){
                this.DAO.eliminar(productoSeleccionado);
                UtilWeb.mensajeInformacion(UtilWeb.obtenerPropiedad("productoController.eliminarExito", this.productoSeleccionado.getProducto_id()),UtilWeb.obtenerPropiedad("productoController.eliminarExito", this.productoSeleccionado.getProducto_id()));
                init();
            }else{
                UtilWeb.mensajeAdvertencia(UtilWeb.obtenerPropiedad("productoController.noSeleccionado"), UtilWeb.obtenerPropiedad("productoController.noSeleccionado"));
            }
        } catch (Exception exception) {
            String mensaje = UtilWeb.controlarError(exception, LOGGER);
            UtilWeb.mensajeError(mensaje, mensaje);
        }
    } 
    
    public String listarProductos(){
        init();
        return "/producto/listar.xhtml?faces-redirect=true";
    }  
    public void buscar(){
        try {
            this.productos = DAO.listar(this.filtro.trim());
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }  
    
    public List<Producto> getProductos() {
        return productos;
    }

    public Producto getProductoSeleccionado() {
        return productoSeleccionado;
    }

    public void setProductos(List<Producto> lista) {
        this.productos = lista;
    }

    public void setProductoSeleccionado(Producto productoSeleccionado) {
        this.productoSeleccionado = productoSeleccionado;
    }

    public Producto getProductoGuardar() {
        return productoGuardar;
    }

    public void setProductoGuardar(Producto productoGuardar) {
        this.productoGuardar = productoGuardar;
    }

    public List<SelectItem> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<SelectItem> categorias) {
        this.categorias = categorias;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }
    
}
