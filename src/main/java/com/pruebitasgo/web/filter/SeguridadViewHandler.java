/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.web.filter;

import com.pruebitasgo.web.util.UtilWeb;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author yesferal
 */
@ManagedBean
@SessionScoped
public class SeguridadViewHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(SeguridadViewHandler.class);

	
	public SeguridadViewHandler() {
		 
	}
	
	public void isAdmin(ComponentSystemEvent event) {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		if (session.getAttribute("usuarioInicio")==null) {
                    UtilWeb.redireccionar("/login.xhtml", LOGGER);
		}
	}
	
	

}
